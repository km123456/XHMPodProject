//
//  main.m
//  XHMPodLib
//
//  Created by 1210257083@qq.com on 08/18/2017.
//  Copyright (c) 2017 1210257083@qq.com. All rights reserved.
//

@import UIKit;
#import "kmAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([kmAppDelegate class]));
    }
}
