//
//  CenterModel.h
//  testZJH
//
//  Created by azi on 17/4/28.
//  Copyright © 2017年 yugang123. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CenterModel : NSObject
+ (void)presentVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object;
+ (void)dismissVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object;


+ (void)pushVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object;
+ (void)popVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object;
@end
