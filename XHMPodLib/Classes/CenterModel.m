//
//  CenterModel.m
//  testZJH
//
//  Created by azi on 17/4/28.
//  Copyright © 2017年 yugang123. All rights reserved.
//

#import "CenterModel.h"

@implementation CenterModel
+ (void)presentVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object{
    

    [self pushOrPesentVC:nextVC with:controller pushType:YES with:object];
}

+ (void)dismissVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object{
    
    [self podOrDismissVC:nextVC with:controller pushType:YES with:object];
}

+ (void)pushVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object{
    
    [self pushOrPesentVC:nextVC with:controller pushType:NO with:object];
}
+ (void)popVC:(NSString *)nextVC with:(UIViewController *)controller with:(id)object{
    
    [self podOrDismissVC:nextVC with:controller pushType:NO with:object];
}

#pragma mark --private
+ (void)pushOrPesentVC:(NSString *)nextVC with:(UIViewController *)controller pushType:(BOOL)type with:(id)object{
    
    //nestvc类名
    Class nextClass = NSClassFromString(nextVC);
    UIViewController *nextController = [[nextClass alloc]init];
    
    NSString *setSelectorString = [NSString stringWithFormat:@"set%@:",@"GroupID"];
    SEL setSelector = NSSelectorFromString(setSelectorString);
    
    if ([nextController respondsToSelector:setSelector]) {
        
        [nextController performSelectorOnMainThread:setSelector withObject:object waitUntilDone:YES];
        //        [nextController performSelector:setSelector];
    }
    
    if (!type) {
        if (nextController) {
            [controller.navigationController pushViewController:nextController animated:YES];
        }
        
    }else{
        if (nextController) {
            [controller presentViewController:nextController animated:YES completion:nil];

        }
    }
    
    
}

+ (void)podOrDismissVC:(NSString *)nextVC with:(UIViewController *)controller pushType:(BOOL)type with:(id)object{
    
    Class nextClass = NSClassFromString(nextVC);
    UIViewController *nextController = [[nextClass alloc]init];
    
    NSString *setSelectorString = [NSString stringWithFormat:@"set%@:",@"Firend"];
    SEL setSelector = NSSelectorFromString(setSelectorString);
    
    if ([nextController respondsToSelector:setSelector]) {
        
        [nextController performSelectorOnMainThread:setSelector withObject:object waitUntilDone:YES];
    }
    
    if (!type) {
        [controller.navigationController popViewControllerAnimated:YES];
    }else{
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
    
}

@end
